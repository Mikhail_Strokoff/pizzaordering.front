var currentNavButtonId = 'nav-button-main';

window.onload = function () {
//    document.getElementById('nav-button-main').onclick = function () {
//        nav_button_click('nav-button-main');
//    };
//    document.getElementById('nav-button-create').onclick = function () {
//        nav_button_click('nav-button-create');
//    };
//    document.getElementById('nav-button-orders').onclick = function () {
//        nav_button_click('nav-button-orders');
//    };
};

function nav_button_click(to) {
    var elemFrom = document.getElementById(currentNavButtonId);
    var elemTo = document.getElementById(to);

    elemFrom.removeAttribute('disabled');
    elemFrom.classList.remove('btn-primary');
    elemFrom.classList.add('btn-default');

    elemTo.setAttribute('disabled', true);
    elemTo.classList.remove('btn-default');
    elemTo.classList.add('btn-primary');

    currentNavButtonId = to;
}

function pizza_add(pizza_id, input) {
    alert("Добавлена пицца №" + pizza_id + ", " + input.value + "шт."); //заглушка
};

function change_address(address){
    alert("Адрес '" + address.value + "' успешно установлен! ");
};
